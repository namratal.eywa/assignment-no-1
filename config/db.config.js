const mysql = require('mysql');
//create hee mysql connection
const dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root123',
});
dbConn.connect(function(error){
    if (error) throw error;
    console.log('MySql Connected Successfully');
    dbConn.query("DROP DATABASE IF EXISTS employee", function (error, result) {
        if (error) throw error;
        console.log("Database Deleted");
    });
    dbConn.query("CREATE DATABASE employee", function (error, result) {
        if (error) throw error;
        console.log("Database Created");
    });
    dbConn.query("USE employee", function (error, result) {
        if (error) throw error;
        console.log("Accessing Employee Database ");
        //result.json({status: true, message: 'Database Created Successfully '})
    });
    dbConn.query("CREATE TABLE emp(empid INT PRIMARY KEY auto_increment, empname VARCHAR(50) NOT NULL, contactno INT(10), address VARCHAR(100) NOT NULL, country VARCHAR(50) NOT NULL)",function(error){
        if(error) throw error;
        console.log("Table Created Successfully");
        //result.json({status: true, message: 'Employee Table Created Successfully '})
    });
})
module.exports = dbConn;