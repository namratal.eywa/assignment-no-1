DROP DATABASE IF EXISTS employee;   
CREATE DATABASE IF NOT EXISTS employee;   
USE employee; 

DROP TABLE IF EXISTS emp; 

CREATE TABLE IF NOT EXISTS emp
  ( 
     empid INT PRIMARY KEY auto_increment, 
     empname VARCHAR(50) NOT NULL, 
     contactno INT(10),
     address VARCHAR(100) NOT NULL,
     country VARCHAR(50) NOT NULL
  ); 