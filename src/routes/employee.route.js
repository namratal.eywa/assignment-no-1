const express = require('express');
const router = express.Router();

const employeeController = require('../controllers/employee.controller');
//get all employees
router.get('/', employeeController.getEmployeeList);
//get employee by id
router.get('/:empid', employeeController.getEmployeeByID);
// create new employee
router.post('/', employeeController.createNewEmployee);
// update employee 
router.put('/:empid', employeeController.updateEmployee);
// delete employee
router.delete('/:empid', employeeController.deleteEmployee);
module.exports = router; 