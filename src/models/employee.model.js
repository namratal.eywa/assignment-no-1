var dbConn = require('../../config/db.config');
//var database = require('../..config/db.sql');
var Employee = function(employee){
    this.empname = employee.empname;
    this.contactno = employee.contactno;
    this.address = employee.address;
    this.country = employee.country;
}
// get all employees
 Employee.getAllEmployees = (result) =>{
     dbConn.query('SELECT * FROM emp', (err, res)=>{
         if(err){
             console.log('Error while fetching employees', err);
             result(null,err);
         }else{
             console.log('Employees Fetched Successfully');
             result(null,res);
         }

     })
 }
// get employee by id from db
Employee.getEmployeeByID = (empid,result)=>{
    dbConn.query('SELECT * FROM emp WHERE empid=?', empid,(err, res)=>{
        if(err){
            console.log('Error while fetching employee by id', err);
            result(null,err);
        }else{
            result(null,res);
        }
    })
}
// create new employee 
Employee.createEmployee = (employeeReqData, result)=>{
  dbConn.query(' INSERT INTO emp SET ? ', employeeReqData, (err, res)=>{
     if(err){
         console.log('Error while inserting data');
         result(null, err);
     }else{
         console.log('Employee inserted successfully');
         result(null, {status: true, message: 'Employee created successfully', insertId:res.empid});
     }
 })
}
// update employee
Employee.updateEmployee = (empid, employeeReqData, result)=>{
    dbConn.query(" UPDATE emp SET empname=?, contactno=?, address=?, country=? WHERE empid=? ", [employeeReqData.empname, employeeReqData.contactno, employeeReqData.address, employeeReqData.country, empid], (err,res)=>{
        if(err){
            console.log('Error while updating the employee');
            result(null,err);
        }else{
            console.log("Employee Updated successfully");
            result(null,res);
        }
    });
  }
// delete employee
Employee.deleteEmployee = (empid, result)=>{
    dbConn.query('DELETE FROM emp WHERE empid=?', [empid], (err, res)=>{
        if(err){
            console.log('Error while deleting');
            result(null,err);
        }else{
            result(null,res);
        }
    })
}
 module.exports = Employee;
 