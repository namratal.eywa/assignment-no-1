//const Employee = require('../models/employee.model');
const EmployeeModel = require('../models/employee.model');

//get all employee list
exports.getEmployeeList = (req,res)=>{
    //console.log('here all employees list');
    EmployeeModel.getAllEmployees((err, employees)=>{
        console.log('we are here');
        if(err)
        res.send(err);
        console.log('Employees', employees);
        res.send(employees)
    })
}

// get employee by ID
exports.getEmployeeByID = (req,res)=>{
    //console.log('get emp by ID');
    EmployeeModel.getEmployeeByID(req.params.empid, (err,employee)=>{
        if(err)
        res.send(err);
        console.log('single employee data ', employee);
        res.send(employee);
    })
}
// create new employee
exports.createNewEmployee = (req, res) =>{
    console.log('Create New Employee');
    const employeeReqData= new EmployeeModel(req.body);
    console.log('employeeReqData',employeeReqData);
   // check null
   if(req.body.contructor === Object && Object.keys(req.body).length === 0){
       res.send(400).send({success: false, message: 'please fill all fields'});
    }else{
        console.log('valid data');
       // return;
        EmployeeModel.createEmployee(employeeReqData,(err, employee)=>{
            if(err)
            res.send(err); 
            res.json({status: true, message: 'Employee Created Successfully ', data: employee})
        })
    }
}
// Update employee
exports.updateEmployee = (req, res)=>{
    const employeeReqData= new EmployeeModel(req.body);
    console.log('employeeReqData Update',employeeReqData);
    //check null
    if(req.body.contructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'please fill all fields'});
    }else{
        console.log('valid data');
        //return;
        EmployeeModel.updateEmployee(req.params.empid, employeeReqData,(err, employee)=>{
            if(err)
            res.send(err); 
            res.json({status: true, message: 'Employee Updated Successfully '})
        })
    }
}
// delete employee
exports.deleteEmployee = (req,res)=>{
    EmployeeModel.deleteEmployee(req.params.empid, (err,employee)=>{
        if(err)
        res.send(err);
        console.log("Employee deleted ");
        res.json({success: true, message: ' Employee deleted successfully!'});
    })
}